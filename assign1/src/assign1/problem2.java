//File:         problem2.java       
//Author:       McKenna Fox
//Date:         September 16th 2018
//Course:       CPS100
//
//Problem Statement:
// Write a program that prompts for and reads a double value representing
// a monetary amount. Then determine the fewest number of each bill and 
// coin needed to represent that amount, starting with the highest 
// (assume that a ten-dollar bill is the maximum size needed) For example,
// if the entered in $47.63 (forty-seven dollars and sixty-three cents),
// then the program should print the equivalent amount as:
// 4 ten dollar bills , 1 five dollar bill, 2 one dollar bills and 2 quarters.
//
//Inputs: monetary value   
//Outputs:  fewest number of bills and coins to represent the monetary value

package assign1;
import java.util.Scanner;
public class problem2

{

  public static void main(String[] args)

  {    
      double amount;
    
      int one, five, ten;
    
      int pennies, nickles, dimes, quarters;
      
      System.out.print("Enter Amount: ");
      Scanner scan=new Scanner(System.in);
    
      amount = scan.nextDouble();

      ten = (int) amount/10;
      amount = amount%10;
    
      five = (int) amount/5;
      amount = amount%5;

      one = (int) amount/1;
      amount = amount%1;

      quarters = (int) (amount/0.25);
      amount = amount%25;

      dimes = (int) (amount/0.10);
      amount = amount%10;
    
      nickles = (int) (amount/0.05);
      amount = amount%5;
    
      pennies = (int) (amount/0.01);
      amount = amount%1;
    
      System.out.println(ten + " ten(s).\n\n");
      System.out.println(five + " five(s).\n\n");
      System.out.println(one + " one(s).\n\n");
      System.out.println(quarters + " quarter(s).\n\n");
      System.out.println(dimes + " dime(s).\n\n");
      System.out.println(nickles + " nickle(s).\n\n");
      System.out.println(pennies + " pennie(s).\n\n");
    
    // TODO Auto-generated method stub

  }

}


